CC=gcc
CFLAGS=-lpthread -I.
INC = semmac.h
OBJECTS= semmac.o main.o
PROGRAM= programa

$(PROGRAM): $(OBJECTS)
	$(CC) $^ -o $@ $(CFLAGS)

%.o: %.c $(INC)
	$(CC) -c $< -o $@ $(CFLAGS)

clean:
	rm $(PROGRAM) $(OBJECTS)
