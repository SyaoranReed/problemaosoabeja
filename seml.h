#ifndef SEM_L
  #define  SEM_L
  #ifdef __APPLE__
    void init(sem_t **sem, char* pathname, int valor_inicial);
  #elif
    void init(sem_t *sem, int valor_inicial);
  #endif
  void p(sem_t *sem);
  void v(sem_t *sem);
#endif
