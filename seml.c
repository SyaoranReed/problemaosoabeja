#include "sem.h"
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>

#ifdef __APPLE__
void init(sem_t **sem, char* pathname, int valor_inicial) {
   if ((*sem = sem_open(pathname, O_CREAT, 0664, valor_inicial)) == SEM_FAILED) {
      printf("Error inicializando semaforo\n");
      exit(EXIT_FAILURE);
   }
}
#elif
// Semaforos del lab Linux
//------------------------------------------------
void init(sem_t *sem, int valor_inicial) {
   if (sem_init(sem, 0, valor_inicial)) {
      printf("Error inicializando semaforo\n");
      exit(EXIT_FAILURE);
   }
}
#endif

//función encargada de bloquear un semáforo
//si ya estaba bloqueado, se queda a la espera de ser liberado para bloquearlo
void p(sem_t *sem) {
   if (sem_wait(sem)) {
      printf("Error fatal haciendo sem_wait()\n");
      exit(EXIT_FAILURE);
   }
}

//función encargada de liberar un semáforo
//en caso que el semáforo ya esté liberado,
void v(sem_t *sem) {
   if (sem_post(sem)) {
      printf("Error fatal haciendo sem_post()\n");
      exit(EXIT_FAILURE);
   }
}
//-----------------------------------------------------------------------------
