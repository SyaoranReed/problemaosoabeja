#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <semaphore.h>
#include "semmac.h"
#include <signal.h>

#define NOMBRE_MUTEXS "/mutexSimulado"
#define NOMBRE_COMER "/puedeComer"

sem_t *mutexSimulado;
sem_t *puedeComer;

int *porciones;
int hPorciones;
int indicePorcion;
int estaComiendo = 0;


void cerrar(int params){
  l(NOMBRE_COMER);
  l(NOMBRE_MUTEXS);
  printf("\nSe cerro la aplicación\n");
  exit(0);
}

// Reserva memoria dinamica
void * crearMemoriaDinamica(int size){
   return malloc(size);
}

// Libera memoria dinamica
void liberarMemoriaDinamica(void * memoria){
   free(memoria);
}

void crearThread(pthread_t *threadPid, pthread_attr_t *attr, int nthread, void *((*funcion) (void *)), void * params){
  for (int i = 0; i < nthread; i++){
    //printf("thread creado %d %d\n", i, nthread);
    pthread_attr_init(&attr[i]);
    pthread_create(&threadPid[i], &attr[i], funcion, params);
  }
}

void esperarThread(pthread_t *threadPID, int nthread){
  for (int i = 0; i < nthread; i++){
     pthread_join(threadPID[i], NULL);
  }
}

void * abeja(void *param){
  int num;
  while(1){
    p(mutexSimulado);
    if(!estaComiendo){
      num = indicePorcion+1;
      porciones[indicePorcion] = num;
      printf("Thread: %lu - Abeja produce la porcion %d\n",pthread_self(), num);
      indicePorcion++;
      if(indicePorcion % hPorciones == 0) {
        estaComiendo = 1;
        indicePorcion = 0;
        v(puedeComer);
      }
    }  
    v(mutexSimulado);
    sleep(rand() % 3);
  }
  pthread_exit(0);
}

void * oso(void * params){
  int i = 0;
  while(1){
    if(estaComiendo) {
      printf("Thread: %lu - Oso come la porcion %d \n",pthread_self() ,porciones[indicePorcion]);
      sleep(rand() % 3);
      indicePorcion++;
      if(indicePorcion % hPorciones == 0) {
        estaComiendo = 0;
        indicePorcion = 0;
        p(puedeComer);
      }
    }
  }
  pthread_exit(0);
}

int main(int argc, char const *argv[])
{
  if (argc != 3){
    printf("Entrada erronea se espera: <app> n H\n");
    exit(1);
  }
  int nAbejas = atoi(argv[1]);
  hPorciones = atoi(argv[2]);
  indicePorcion = 0;
  srand(time(0));

  //Sobreescribo la funcion que se va a ejecutar al presionar CTRL-C
  //Para que libere los semaforos
  signal(SIGINT, cerrar);
  init(&puedeComer, NOMBRE_COMER, 0);
  init(&mutexSimulado, NOMBRE_MUTEXS, 1);
  //
  porciones = crearMemoriaDinamica(sizeof(int) * hPorciones);
  pthread_t *pidAbeja = crearMemoriaDinamica(sizeof(pthread_t) * nAbejas);
  pthread_t pidOso;
  pthread_attr_t *attrAbeja = crearMemoriaDinamica(sizeof(pthread_attr_t) * nAbejas);
  pthread_attr_t attrOso;
  crearThread(&pidOso, &attrOso, 1, oso, NULL);
  crearThread(pidAbeja, attrAbeja, nAbejas, abeja, NULL);
  esperarThread(&pidOso, 1);
  esperarThread(pidAbeja, nAbejas);
  liberarMemoriaDinamica(pidAbeja);
  liberarMemoriaDinamica(attrAbeja);
  pthread_exit(0);

  return 0;
}
